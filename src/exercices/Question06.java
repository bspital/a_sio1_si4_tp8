
package exercices;

import donnees.Personne;

public class Question06 {

    public static void main(String[] args) {
        float minpoids=0;
        for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("M")) {
              if(pers.poids<minpoids || minpoids==0) {
                  minpoids = pers.poids;
              }
           }
        } 
        System.out.printf("Poids le plus faible parmi les judokas: %2.0fkg\n",minpoids);
    }
}

