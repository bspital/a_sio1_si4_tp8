
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question03 {

    public static void main(String[] args) {
        System.out.println("Liste des membres masculins du club de plus de 80kg:\n");
        
        trierLesPersonnesParNomPrenom(listeDesPersonnes);
        
        for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("M") && pers.poids >= 80) {
              System.out.printf("%-10s %-10s %3dkg %-20s\n",pers.nom,pers.prenom,pers.poids,pers.ville);
           }
            
        } 
    }
}

