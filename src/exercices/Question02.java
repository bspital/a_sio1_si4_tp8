
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question02 {

    public static void main(String[] args) {
        
        System.out.println("Liste des membres féminins du club:\n");
        
        trierLesPersonnesParNomPrenom(listeDesPersonnes);
        
        for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("F")) {
              System.out.printf("%-10s %-15s\n",pers.nom,pers.prenom);
           }
            
        } 
              
    }
}

