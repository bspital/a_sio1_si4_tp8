package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.dateVersChaine;
import static utilitaires.UtilDojo.determineCategorie;

public class Question08 {

  public static void main(String[] args) {
      System.out.println("Liste des membres masculins de la catégorie légers âgés de 20 ou plus au: "+utilitaires.UtilDate.aujourdhuiChaine());
        for (Personne pers : donnees.Club.listeDesPersonnes){
           int age= ageEnAnnees(pers.dateNaiss);
           if (pers.sexe.equals("M") && age >= 20 && "légers".equals(determineCategorie( pers.sexe, pers.poids ))) {
               System.out.printf("%-10s %-10s %-9s %-2dans %2dkg %-10s\n",pers.nom, pers.prenom, dateVersChaine(pers.dateNaiss), age, pers.poids, pers.ville);
           }
        } 
        
   }
}



