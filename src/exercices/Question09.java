package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.dateVersChaine;
import static utilitaires.UtilDojo.determineCategorie;

public class Question09 {

  public static void main(String[] args) {
     float agetotal=0, nbpers=0;
     for (Personne pers : donnees.Club.listeDesPersonnes){
           int age= ageEnAnnees(pers.dateNaiss);
           if ("légers".equals(determineCategorie( pers.sexe, pers.poids ))) {
               agetotal+=age;
               nbpers++;
           }
     }
      System.out.printf("Moyenne d'âge des membres du club de la catégorie léger: %-2.2fkg\n",agetotal/nbpers);
    
   }
}



