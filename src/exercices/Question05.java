
package exercices;

import donnees.Personne;

public class Question05 {

    public static void main(String[] args) {
        float totalpoids=0;
        int femme = 0;
        for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("F")) {
              totalpoids+=pers.poids;
              femme++;
           }
        } 
        System.out.printf("Moyenne des poids des judokas féminins: %2.2fkg\n",totalpoids/femme);
    }
}

