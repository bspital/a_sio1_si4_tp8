
package exercices;

import donnees.Personne;


public class Question04 {

    public static void main(String[] args) {
        int homme=0, femme=0;
        System.out.println("Effectif du club:\n");
         for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("M")) {
              homme++;
           }
           else {
               femme++;
           }
        } 
         System.out.printf("Effectif masculin: %2d\n",homme);         
         System.out.printf("Effectif feminin: %3d\n",femme);         
         System.out.printf("\nEffectif total: %3d\n",femme+homme);


    }
}

