
package exercices;

import donnees.Personne;

public class Question07 {

    public static void main(String[] args) {
        float minpoids=0;
        String nom = null, prenom = null;
        for (Personne pers : donnees.Club.listeDesPersonnes){
           if (pers.sexe.equals("M")) {
              if(pers.poids<minpoids || minpoids==0) {
                  minpoids = pers.poids;
                  nom = pers.nom;
                  prenom = pers.prenom;
              }
           }
        } 
        System.out.printf("Judoka masculin le plus lèger: %-10s %-10s %2.0fkg\n",prenom,nom,minpoids);
    }
}



