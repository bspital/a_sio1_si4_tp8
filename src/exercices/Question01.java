
package exercices;

import donnees.Personne;


public class Question01 {
    
   public static void main(String[] args) {
         
      
       System.out.println("Liste des membres du Club");
       
       
       
       for ( Personne  pers : donnees.Club.listeDesPersonnes   ){
           
           
           System.out.printf("%-15s %-15s %-1s poids %4d kg %-20s %-2d\n",
                                 pers.nom,
                                 pers.prenom,
                                 pers.sexe,
                                 pers.poids,
                                 pers.ville,
                                 pers.nbVictoires);
       
       }
       
       
   
        
    }
}






