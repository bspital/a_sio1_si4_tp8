package donnees;

import java.util.Date;

// Définition d'une "fiche" Personne

public class Personne {
   
   public String    nom; 
   public String    prenom;
   public String    sexe;
   public Date      dateNaiss;
   public int       poids;
   public String    ville;
   public int       nbVictoires; 
   
   
   //<editor-fold defaultstate="collapsed" desc="Redéfinition de la méthode toString">
   
//   @Override
//   public String toString() {
//    
//       return String.format("%-15s  %-15s", nom, prenom);
//   
//   }
//   
   
   //</editor-fold>
   
   
   
}


