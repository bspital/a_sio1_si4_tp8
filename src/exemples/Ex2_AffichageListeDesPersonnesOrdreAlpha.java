package exemples;

// c'est dans donnees.Club que se trouve la liste listeDesPersonnes 
// c'est dans donnees.Personne que se trouve la définition du type Personne 
// c'est dans utilitaires.UtilTriListe que se trouve la fonction trierLesPersonnesParNomPrenom

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Ex2_AffichageListeDesPersonnesOrdreAlpha {

    public static void main(String[] args) {
        
        System.out.println("\n\n"); 
        
        // On utilise la fonction trierLesPersonnesParNomPrenom
        // se trouvant dans utilitaires.UtilTriListe ( voir l'import en ligne 9)
         
        trierLesPersonnesParNomPrenom(listeDesPersonnes);
        
        for ( Personne pers : listeDesPersonnes){
        
            // on affiche les champs nom et pers de la variable pers grâce aux notations pointées
            // pers.nom  et pers.prenom
            
            System.out.printf("%-20s %-20s\n", pers.nom, pers.prenom);
               
            // Affichage formaté de deux variables pers.nom et pers.prenom
            // Que signifie "%-20s %-20s\n" ?
            // le premier %-20s sera remplacé à l'exécution par la valeur de pers.nom
            // % indique une règle d'affichage 
            // -20s indique qu'on réserve une colonne de 20 caratères pour afficher une chaîne
            // de caratères ( s signifie String)
            // le - indique que la chaîne est cadrée à gauche de la colonne
            
            // le deuxième %-20s sera remplacé par la valeur de pers.prenom 
            // avec les mêmes règles d'affichage      
        
            // \n provoque un passage à la ligne   
        }
        System.out.println("\n\n");  
    }      
}   