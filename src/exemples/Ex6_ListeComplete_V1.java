package exemples;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static utilitaires.UtilDate.dateVersChaine;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Ex6_ListeComplete_V1 {

  public static void main(String[] args) {
        
     System.out.printf("\nComposition du club de Judo au: %10s \n\n",aujourdhuiChaine());
    
     trierLesPersonnesParNomPrenom(listeDesPersonnes);
    
     for(Personne pers : listeDesPersonnes) {    
    
       System.out.printf(
                   
          "%-15s %-15s %-2s né(e) le: %-10s %2d ans poids %4d kg catégorie: %-15s %2d victoires %-20s\n",
           
          pers.nom,
          pers.prenom, 
          pers.sexe,
          dateVersChaine(pers.dateNaiss),
          ageEnAnnees(pers.dateNaiss),
          pers.poids,
          utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids ),
          pers.nbVictoires,
          pers.ville
       );           
     }    
     System.out.println();       
  }
}


